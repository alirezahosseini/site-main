import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';
import Footer from 'components/Footer';
import {
  ProjectContainer, ProjectBackground, ProjectHeader, ProjectSection,
  ProjectSectionContent, ProjectImage} from 'components/ProjectLayout';
import { useScrollRestore } from 'hooks';
import { media } from 'utils/style';
import prerender from 'utils/prerender';
import modernBackground from 'assets/modern-background.jpg';
import modernBackgroundLarge from 'assets/modern-background-large.jpg';
import modernBackgroundPlaceholder from 'assets/modern-background-placeholder.jpg';
import modern from 'assets/modern.jpg';
import modernLarge from 'assets/modern-large.jpg';
import modernPlaceholder from 'assets/modern-placeholder.jpg';

const title = 'Project Persia Beard Club';
const description = 'Building a community that puts all beard styles and and lovers together.';
const roles = [
  'Visual Identity',
  'UX and UI Design',
  'Full-stack Development',
];

function ProjectModern() {
  useScrollRestore();

  return (
    <Fragment>
      <Helmet>
        <title>{`Projects | ${title}`}</title>
        <meta name="description" content={description} />
      </Helmet>
      <ProjectContainer>
        <ProjectBackground
          srcSet={`${modernBackground} 1000w, ${modernBackgroundLarge} 1920w`}
          placeholder={modernBackgroundPlaceholder}
          entered={!prerender}
        />
        <ProjectHeader
          title={title}
          description={description}
          url="https://persiabeardclub.com"
          roles={roles}
        />
        <ProjectSection>
          <ProjectSectionContent>
            <ProjectImage
              reveal
              srcSet={`${modern} 800w, ${modernLarge} 1440w`}
              placeholder={modernPlaceholder}
              sizes={`(max-width: ${media.mobile}px) 500px, (max-width: ${media.tablet}px) 800px, 1000px`}
              alt="Landing screen of the Persia Beard Club website."
            />
          </ProjectSectionContent>
        </ProjectSection>
      </ProjectContainer>
      <Footer />
    </Fragment>
  );
}

export default ProjectModern;
