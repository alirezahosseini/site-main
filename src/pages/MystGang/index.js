import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';
import { useScrollRestore } from 'hooks';
import Footer from 'components/Footer';
import {
  ProjectContainer, ProjectBackground, ProjectHeader, ProjectSection,
  ProjectSectionContent, ProjectImage} from 'components/ProjectLayout';
import { media } from 'utils/style';
import prerender from 'utils/prerender';
import mystgangBackground from 'assets/mystgang-background.jpg';
import mystgangBackgroundLarge from 'assets/mystgang-background-large.jpg';
import mystgangBackgroundPlaceholder from 'assets/mystgang-background-placeholder.jpg';
import mystgang from 'assets/mystgang.jpg';
import mystgangLarge from 'assets/mystgang-large.jpg';
import mystgangPlaceholder from 'assets/mystgang-placeholder.jpg';
import './index.css';

const title = 'Mostafa Rahmani';
const description = 'A personal site for the photography content creator known as Mostafa Rahmani.';
const roles = [
  'Branding & Identity',
  'UX and UI Design',
  'Front-end Development',
];

function MystGang() {
  useScrollRestore();

  return (
    <Fragment>
      <Helmet>
        <title>{`Projects | ${title}`}</title>
        <meta name="description" content={description} />
      </Helmet>
      <ProjectContainer>
        <ProjectBackground
          srcSet={`${mystgangBackground} 1000w, ${mystgangBackgroundLarge} 1920w`}
          placeholder={mystgangBackgroundPlaceholder}
          entered={!prerender}
        />
        <ProjectHeader
          title={title}
          description={description}
          url="https://mostafarahmani.com/"
          roles={roles}
        />
        <ProjectSection>
          <ProjectSectionContent>
            <ProjectImage
              reveal
              srcSet={`${mystgang} 800w, ${mystgangLarge} 1440w`}
              placeholder={mystgangPlaceholder}
              sizes={`(max-width: ${media.mobile}px) 500px, (max-width: ${media.tablet}px) 800px, 1000px`}
              alt="Landing screne of the Mostafa Rahmani website."
            />
          </ProjectSectionContent>
        </ProjectSection>
      </ProjectContainer>
      <Footer />
    </Fragment>
  );
}

export default MystGang;
