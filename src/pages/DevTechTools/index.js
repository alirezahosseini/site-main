import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet-async';
import Footer from 'components/Footer';
import {
  ProjectContainer, ProjectBackground, ProjectHeader, ProjectSection,
  ProjectSectionContent, ProjectImage} from 'components/ProjectLayout';
import { useScrollRestore } from 'hooks';
import { media } from 'utils/style';
import prerender from 'utils/prerender';
import dttBackground from 'assets/dtt-background.jpg';
import dttBackgroundLarge from 'assets/dtt-background-large.jpg';
import dttBackgroundPlaceholder from 'assets/dtt-background-placeholder.jpg';
import dtt from 'assets/dtt.jpg';
import dttLarge from 'assets/dtt-large.jpg';
import dttPlaceholder from 'assets/dtt-placeholder.jpg';
const title = 'Khalifa Cafe';
const description = 'I lead the design and development of Khalifa Cafe. We focused on creating the best Ecommerce experience for Clients and customers to enjoy their shopping.';
const roles = [
  'Visual Identity',
  'UX and UI Design',
  'Full-stack Development',
];

function DevTechTools() {
  useScrollRestore();

  return (
    <Fragment>
      <Helmet>
        <title>{`Projects | ${title}`}</title>
        <meta name="description" content={description} />
      </Helmet>
      <ProjectContainer>
        <ProjectBackground
          srcSet={`${dttBackground} 1000w, ${dttBackgroundLarge} 1920w`}
          placeholder={dttBackgroundPlaceholder}
          entered={!prerender}
        />
        <ProjectHeader
          title={title}
          description={description}
          url="http://khalifacs.com"
          roles={roles}
        />
        <ProjectSection>
          <ProjectSectionContent>
            <ProjectImage
              reveal
              srcSet={`${dtt} 800w, ${dttLarge} 1440w`}
              placeholder={dttPlaceholder}
              sizes={`(max-width: ${media.mobile}px) 500px, (max-width: ${media.tablet}px) 800px, 1000px`}
              alt="Landing screen of the khalifa cafe website."
            />
          </ProjectSectionContent>
        </ProjectSection>
      </ProjectContainer>
      <Footer />
    </Fragment>
  );
}

export default DevTechTools;
