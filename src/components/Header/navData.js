export const navLinks = [
  {
    label: 'Projects',
    pathname: '/',
    hash: '#project-1',
  },
  {
    label: 'Articles',
    pathname: '/articles',
  },
  {
    label: 'About',
    pathname: '/',
    hash: '#about',
  },
  {
    label: 'Contact',
    pathname: '/contact',
  },
];

export const socialLinks = [
  {
    label: 'LinkedIn',
    url: 'https://www.linkedin.com/in/alireza-ghaffari-95a75065/',
    icon: 'linkedin',
  },
  {
    label: 'Github',
    url: 'https://github.com/alirezahosseini',
    icon: 'github',
  },
  {
    label: 'Email',
    url: 'mailto:ali.g.hosseini@gmail.com',
    icon: 'email',
  },
  {
    label: 'Twitter',
    url: 'https://twitter.com/alirezakael',
    icon: 'twitter',
  },
];
