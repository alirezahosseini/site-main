import React from 'react';
import Anchor from 'components/Anchor';
import './index.css';

const Footer = () => (
  <footer className="footer">
    <span className="footer__date">
      {`© 2014-${new Date().getFullYear()} `}
    </span>
    <Anchor className="footer__link" secondary href="/humans.txt">
      Paarlaa Team
    </Anchor>
  </footer>
);

export default Footer;
